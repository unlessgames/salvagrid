# welcome to **salvagrid**!

a simple webgl implementation of an [idea](https://community.vcvrack.com/t/diy-marbles-based-physical-matrix-mixer-controller-project) from [Aria Salvatrice](https://aria.dog)

you can create a grid with real-world objects, it detects them with a webcam and sends CC values through a MIDI output to control other software

can be used to control a [matrix mixer](https://library.vcvrack.com/?query=matrix&brand=Bogaudio&tag=&license=) inside [VCVRack](https://vcvrack.com) (as proposed in the original concept) or to launch clips / mute tracks with ableton etc.


each cell in the grid can be either 
  **on** or **off**

they translate to MIDI CC messages
  **127** and **0**


# you need

  - **midi output port**
      (it should show up at *device* under the **MIDI** settings)

      if you don't have any
        windows - see [loopMIDI](https://www.tobias-erichsen.de/software/loopmidi.html)
        osx - try [this](https://support.apple.com/guide/audio-midi-setup/transfer-midi-information-between-apps-ams1013/mac)
        linux - try [this](http://tldp.org/HOWTO/MIDI-HOWTO-10.html)

  - **webcam**
      you'd want a webcam you can place so it's facing your background

  - **desaturated background**
      for example a white paper and a rectangular grid drawn onto it with a black pen

  - **colorful items**
      they should be large enough to cover most of a cell inside your grid

      I've used pieces of colored building blocks 
      ([steckblumen](https://www.google.com/search?tbm=isch&q=steckblumen) to be exact)


# to setup the grid

  your grid should be a **rectangle**, but trapezoids can work as well since you can skew the selection

  - select the **corners**
      drag them on the camera image to match your grid
      they should be aligned as

    ```
      A --- B
      |     |
      |     |
      D --- C
    ```

  - set your grid's size 
      *columns* and *rows*

  - pick your **MIDI** output *device*

  - pick a **MIDI** *channel* (default : 1)

  - pick the *start* of the grid
      this will determine which **CC** values to send

      if a cells number goes over 119 the grid will output on the next MIDI *channel* after the one you've selected (show as ^0, ^1, ^2 etc.)

  - tweak the values under **DETECTOR** 
    (the numbered table under the MIDI settings should be stable without flickering)

    *saturation*
    sets the threshold (in saturation) after cells turn **on**
    *brightness*
    sets the threshold needed for cells to reach in brightness to turn **on**


  at the bottom there are different renders of the detection

  - a high resolution **preview** of the highlighted image to help with tweaking

  - the **oversampled** source

  - the **1-pixel** source

  the detector outputs **on** or **off** based on a combination of what is happening on the **oversampled** and the **1-pixel**

# todo
  - get available resolutions from webcam
  - other modes of detection
      eg. based on brightness/darkness
  - multiple grids at once
  - smooth cell CC values
      hue to value for example