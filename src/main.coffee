options = window.options
width = window.innerWidth
height = window.innerHeight

T = THREE
V2 = T.Vector2
V3 = T.Vector3

default_corners = () ->
  s = 20
  e = options.screen.h - 20
  [
    new V2(s, e)
    new V2(e, e)
    new V2(e, s)
    new V2(s, s)
  ]
default_board = () ->
  columns : 4
  rows : 4
  corners : default_corners()
  midi : 
    device : options.midi_outputs[0]
    channel : 1
    start : 0

camera = null
renderer = null
render_target = null
cam_image = null
cursor = null
# cursor_color = new Float32Array([])

gui = null
options_gui = null

detectors = []

boards = []

dragged = null
drag_offset = new V2(0,0)

scene = new THREE.Scene()
sceneScreen = new THREE.Scene()

corner_letters = options.corner_names.split("").map (n) -> new T.Geometry()

video = document.getElementById 'video'
video_texture = null

create_capture = (ready) ->
  if navigator.mediaDevices and navigator.mediaDevices.getUserMedia
    constraints = 
      video : 
        width : options.screen.w
        height: options.screen.h
        facingMode: 'user'

    navigator.mediaDevices.getUserMedia( constraints ).then(( stream ) ->
      video.srcObject = stream;
      video.play();
      video_texture = new THREE.VideoTexture video
      ready()
    ).catch(( err ) ->
      error "webcam_permission"
    );
  else
    error "webcam"

init_midi = (ready) ->
  WebMidi.enable((err) ->
    if err?
      error "webmidi"
    else
      if WebMidi.outputs.length > 0
        options.midi_outputs = WebMidi.outputs.map((o) -> o.name)
        ready()
      else
        error "midi"
  )

manager = new THREE.LoadingManager()
manager.onProgress = (url, items_loaded, total) ->
manager.onLoad = () ->
  init_help()
  create_capture () -> init_midi init

file_loader = new THREE.FileLoader manager
font_loader = new THREE.FontLoader manager

load = () ->  
  materials.load file_loader
  file_loader.load "README.md", (data) -> options.help = data
  font_loader.load( './font/terminus.json', ( font ) ->
    font_options = 
      font: font,
      size: options.font_size,
      height: 5,
      curveSegments: 12,
      bevelEnabled: false,
    corner_letters = options.corner_names.split("").map (n) -> new T.TextGeometry( n, font_options)
  )

class Togglui
  fun : () ->
    t = @object[@name]
    @object[@name] = not t
    @gu.name @texts[if t then 1 else 0]
    $(@gu.domElement.parentNode.parentNode).toggleClass "disabled", t
    @callback()
  constructor : (@gui, @object, @name, @texts, @initial, @callback) ->
    @gu = @gui.add(@, "fun").name(@texts[if @initial then 0 else 1])



error = (type) ->
  $("#help-container").removeClass("closed")
  $("#help").html barkdown.render(options.error_messages.header + options.error_messages[type])
  

closest = (mp, ps, b = 0) ->
  ps.map((p, i) -> {p : p, i : i, board : b}).sort((a, b) -> if a.p.distanceTo(mp) < b.p.distanceTo(mp) then -1 else 1)[0]

stringify_vector = (v) ->
  JSON.stringify({x : v.x, y : v.y})
local_save = (x, name) ->
  window.localStorage.setItem name, JSON.stringify(if Array.isArray x then x.map((a) -> stringify_vector a) else x)
save_board = (index) ->
  local_save boards[index].save(), "board#{index}"
local_load = (name, x) ->
  l = window.localStorage.getItem name
  if l? 
    parsed = JSON.parse(l)
    if parsed?
      parsed
    else
      x
  else
    x


events = 
  keydown : (k) ->
  mousedown : (e) ->
    if e.ctrlKey
    else if e.shiftKey
    else
      if Mouse.x >= 0 and Mouse.x < width and Mouse.y >= 0 and Mouse.y < height
        ps = boards.reduce(((a, v, i) -> a.concat(v.corners)), [])
        dragged = closest(new V2(Mouse.x, Mouse.y - (height - options.screen.h)), ps)
        drag_offset = new V2(dragged.p.x - Mouse.x, (dragged.p.y) - (Mouse.y  - (height - options.screen.h)))
  mouseup : (e) ->
    if dragged?
      local_save boards[dragged.board].corners, "selection"
      dragged = null
    for b in boards
      b.manual_release()

  mousemove : (e) ->
    ho = height - options.screen.h
    if dragged?
      i = dragged.i % 4
      boards[Math.floor(dragged.i / 4)].drag i, Mouse.x + drag_offset.x, Mouse.y + drag_offset.y
      cursor.position.set Mouse.x, Mouse.y, 2
    else
      if boards.length > 0
        c = closest new V2(Mouse.x, Mouse.y - ho), boards[0].corners
        if c
          cursor.position.set c.p.x, c.p.y + ho, 2

    # cursor_color = new Float32Array 4
    # renderer.readRenderTargetPixels render_target, Mouse.x, Mouse.y, 1, 1, cursor_color

grid_geom = (ps, sw, sh, w, h) ->
  pss = ps.slice(0, 3).concat(ps.slice(2, 4)).concat [ ps[0] ]
  g = new THREE.BufferGeometry()
  vertices = new Float32Array( [
    0,  0,  0
    sw,  0,  0
    sw,  sh,  0

    sw,  sh,  0
    0,  sh,  0
    0,  0,  0
  ] );  
  g.setAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) )

  uvs = new Float32Array(
    pss.reduce((a, v, i) ->
      a.concat [v.x / w, v.y / h]
    , [])
  )
  g.setAttribute( 'uv', new THREE.BufferAttribute( uvs, 2 ) )
  g

save_options = () ->
  local_save options, "options"

toggle_help = (show) ->
  if not show?
    show = $("#help-container").hasClass "closed"
  $("#help-container").toggleClass "closed", not show
  local_save not show, "closed_help"
  save_options()

init_help = () ->
  options.closed_help = local_load("closed_help", options.closed_help)

  options.help = barkdown.render(options.help) + 
      "<br>made by <a href='https://unlessgames.com/?t=d' target='_blank'>unless games</a><br>" + 
      "<br>click <a onclick='close_help()'>here</a> to minimize this!<br>" +
      "<br>have fun!<br>"

  $("#help-container")
    .toggleClass "closed", options.closed_help
    .html $("<div>", html : options.help, class : "display", id: "help")
  $("#help-button").click () -> toggle_help()


init_gui = () ->
  gui = new dat.GUI autoPlace : false
  $("#gui-container").html gui.domElement

  options_gui = gui.addFolder "OPTIONS"

  options_gui.add options, "resolution", options.resolutions
    .onChange (v) =>
      save_options()
      window.location.reload()

  new Togglui(options_gui, options, "show_camera", 
    ["disable rendering", "enable rendering"], true, 
    () -> renderer.clear()
  )

  r = 
    reset_defaults : () ->
      window.localStorage.clear()
      local_save options.closed_help, "closed_help"
      window.location.reload()

  options_gui.add r, "reset_defaults"
  options_gui.add options, "refresh_cycle", 1, 128
    .step 1
    .onChange (v) =>
      save_options()

  options_gui.add(options, "detector", options.detector_list).onChange((v) =>
    save_options()
    window.location.reload()
  ).name "detector mode"

  options_gui.open()
  

  materials.init_gui gui
  materials[options.detector].gui.show()

  # gui.add(window, "add_new_grid").name("+ NEW GRID")

close_help = () ->
  toggle_help false
  local_save true, "closed_help"

bind_events = () ->
  window.addEventListener( 'resize', resize, false );
  Keys.init window
  Keys.listen(events, "keydown", "press", "all")

  Mouse.init $("canvas").get(0)
  Mouse.listen events, "mousedown", "press", MOUSE_LEFT
  Mouse.listen events, "mouseup", "release", MOUSE_LEFT
  $(window).mouseup events.mouseup
  Mouse.listen events, "mousemove", "move"

init_boards = () ->
  board_data = local_load "board0", default_board()
  boards.push new Board(board_data, [detectors[options.detector], materials.screen.material, materials.line.material], 0, gui)
  for b in boards
    for o in b.objects
      scene.add o
    for o in b.screen_objects
      sceneScreen.add o

create_objects = () ->
  cam_image = new THREE.Mesh(
    new THREE.PlaneBufferGeometry options.screen.w, options.screen.h
    new THREE.MeshBasicMaterial( map: video_texture, side : THREE.DoubleSide )
  )
  cam_image.position.set options.screen.w * 0.5, height - options.screen.h * 0.5 , 0
  cam_image.visible = options.show_camera

  cursor = new THREE.Mesh new THREE.BoxGeometry(20, 20, 1), materials.line.material
  cursor.position.set width * 0.5, height * 0.5

  uvs = [
    new V2 0, 0
    new V2 1, 0
    new V2 1, 1
    new V2 0, 1
  ]
  quad = new THREE.Mesh( grid_geom(uvs, width, height, 1, 1), materials.screen.material )
  quad.position.set 0, 0, 1

  [cam_image, cursor, quad]

init_renderers = () ->
  render_target = new THREE.WebGLRenderTarget( 
    options.plane_width
    options.plane_height
    antialias : false
    minFilter: THREE.NearestFilter
    magFilter: THREE.NearestFilter
    format: THREE.RGBAFormat
    type: THREE.FloatType
  )

  renderer = new THREE.WebGLRenderer(
    antialias: false
    magFilter: THREE.NearestFilter
    minFilter: THREE.NearestFilter
  )
  renderer.setPixelRatio window.devicePixelRatio
  renderer.setSize window.innerWidth, window.innerHeight

  $("body").append renderer.domElement

load_options = () ->
  v = options.version

  saved_options = local_load "options", options

  for k of saved_options
    options[k] = saved_options[k]

  if options.version isnt v
    window.localStorage.clear()
    window.location.reload()

  rs = options.resolution.split "x"
  options.screen.w = parseInt(rs[0])
  options.screen.h = parseInt(rs[1])

  width = window.innerWidth
  height = window.innerHeight
  options.plane_width = width
  options.plane_height = height

init = () ->
  load_options()

  materials.init()

  init_renderers()

  detectors =
    saturator : materials.saturator.material
    grayscale : materials.grayscale.material
  options.detector_list = Object.keys(detectors)

  materials.saturator.update_uniform "tex0", video_texture
  materials.grayscale.update_uniform "tex0", video_texture
  materials.screen.update_uniform "tex0", render_target.texture

  camera = new THREE.OrthographicCamera 0, width, height, 0, -10, 1000
  camera.position.z = -1;

  for o in create_objects()
    sceneScreen.add o

  init_gui()
  
  init_boards()

  bind_events()
  
  animate();

resize = () ->
  window.location.reload()
  # width = window.innerWidth
  # height = window.innerHeight
  # camera = new THREE.OrthographicCamera( 0, width, height, 0, -10, 1000 );
  # for b in boards
  #   b.generate_geoms()
  # renderer.setSize( width, height )

frames = 0
time = 0
last_time = 0
animate = () ->

  requestAnimationFrame animate

  now = new Date().getTime() * 0.001

  dt = now - last_time

  renderer.setRenderTarget render_target
  renderer.clear()
  renderer.render scene, camera

  nt = time + options.speed * dt
  time = nt - Math.floor(nt)

  materials.line.material.uniforms.time.value = time
  materials.line.material.uniformsNeedUpdate = true

  frames++
  if frames % options.refresh_cycle is 0
    for b in boards
      b.update_grid()

  if options.show_camera
    renderer.setRenderTarget null
    renderer.render sceneScreen, camera

  last_time = now


load()

# remove_grid = (i) ->
#   b = boards.splice i, 1
#   grid_gui.__folders["##{i}"] = undefined
#   boards.map((b, i) -> 
#     b.index = i
#     b.generate_geoms()
#   )
# add_new_grid = () ->
#   boards.push new Board options.grid_width, options.grid_height, material, local_load( "selection"), boards.length
#   for b in boards
#     sceneScreen.add b.selection
#     for o in b.objects
#       scene.add o
