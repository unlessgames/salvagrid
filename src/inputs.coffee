MOUSE_LEFT = 0
MOUSE_MIDDLE = 1
MOUSE_RIGHT = 2
_bind = (o, f) -> o[f].bind o

class InputEventListener
  constructor : (@listeners) ->
  listen : (obj, callback, event_type, button = "all") ->
      if @listeners[event_type][button]?
        @listeners[event_type][button].push obj[callback].bind(obj)
      else
        @listeners[event_type][button] = [obj[callback].bind(obj)]
  call_back : (event_type, e, button = "all") ->
    if @listeners[event_type][button]?
      for callback, i in @listeners[event_type][button]
        callback e, button
    if @listeners[event_type].all?
      for callback, i in @listeners[event_type].all
        callback button, e

Keys = 
  current : []
  pressed : null
  listener : new InputEventListener({
    press : {}
    release : {}
  })
  listen : (o, e, t, b) -> @listener.listen o, e, t, b
  down : (k) ->
    i = @current.indexOf k
    i > -1
  shift : () ->
    @current.includes "ShiftLeft" or @current.includes "ShiftRight"
  ctrl : () ->
    @current.includes "ControlLeft" or @current.includes "ControlRight"
  alt : () ->
    @current.includes "AltLeft" or @current.includes "AltRight"
  press : (e) ->

    # if e.shiftKey then e.preventDefault()

    if !@current.find((k) -> k is e.code)?
      started_press = true
      @pressed = e.code
    else 
      started_press = false
    @current = @current.filter((k) -> k isnt e.code)
    @current.push e.code    
    if started_press
      @listener.call_back "press", e, e.code
  release : (e) ->
    @current = @current.filter((k) -> k isnt e.code)
    @listener.call_back "release", e, e.code
  update : () ->
    @pressed = null;
  init : (s) ->
    s.addEventListener "keydown", (e) => @press(e)
    s.addEventListener "keyup", (e) => @release(e)
    # document.addEventListener("keydown", (e) -> Keys.press(e))
    # document.addEventListener("keyup", (e) -> Keys.release(e))

    # document.addEventListener("keyup", (e) -> Keys.release(e))

Mouse = 
  x : 0 
  y : 0
  current : []
  clicked : null
  listener : new InputEventListener( {
    press : {}
    release : {}
    scroll : {}
    move : {}
  })
  listen : (o, c, e = "press", b = "all") -> @listener.listen o, c, e, (if b is "all" then b else "b" + b)
  scroll : (e) -> 
    # e.preventDefault()
    @listener.call_back "scroll", e, ("b" + e.button)
  hold : (b) ->
    @current.find((bn) -> bn is b)?
  look : (e) ->
    pilot.look e.deltax, e.deltay
  pressed : (which) -> 
    @current.find((b) -> if which? then which is b else true)?
  down : (e) ->
    if e.button is 2
      e.preventDefault()
    if !@current.find((b) -> b is e.button)?
      @clicked = e.button
    @current = @current.filter((b) -> b isnt e.button)
    @current.push e.button
    if @clicked?
      @listener.call_back "press", e, ("b" + @clicked)
  up : (e) ->
    @listener.call_back "release", e, ("b" + e.button)
    @clicked = null
    @current = @current.filter((b) -> b isnt e.button)
  move : (e) ->
    # e = e
    @x = e.offsetX
    @y = e.target.height - e.offsetY
    @_x = ( @x / e.target.width ) * 2 - 1
    @_y = (( @y / e.target.height ) * 2 - 1) * -1
    @listener.call_back "move", e, ("b" + e.button)
  update : () ->
    @clicked = null
  init : (s) ->
    s.addEventListener "mousedown", (_bind @, "down")
    s.addEventListener "mouseup", (_bind @, "up")
    s.addEventListener "mousemove", (_bind @, "move")
    s.addEventListener "contextmenu", (_bind @, "down")

    # s.addEventListener("pointermove", (_bind @, "move"))
    # document.addEventListener("mousedown", (_bind @, "down"))
    # document.addEventListener("mousedown", () -> $())
    # document.addEventListener("mouseup", (_bind @, "up"))
    # s.addEventListener('DOMScroll', (_bind @, "scroll"))
    # s.addEventListener('mousewheel', (_bind @, "scroll"))
