barkdown =
  render : (t) ->
    # comments
    t = t.replace(/\<!--(.*)--!>/gm, "")

    #images
    imagify = (x) ->
      u = x.match(/\(([^\)]+)\)/)[1].split(" =")
      size = if u[1]? then u[1] else null
      scaling = 
        if size?
          wh = size.split("x")
          w = wh[0]
          h = if wh.length > 1 then wh[1] else "auto"
          "width=#{w}px height=#{h}px"
        else ""
      url = u[0]
      name = x.match(/\[([^\)]+)\]/)[1]
      "<img src='#{url}' #{scaling}></img>"

    t = t.replace(/!\[([^\[\]]*)\]\((.*?)\)/gm, imagify)

    #links
    linkify = (x) ->
      url = x.match(/\(([^\)]+)\)/)[1]
      name = x.match(/\[([^\)]+)\]/)[1]
      "<a href='#{url}' target='_blank'>#{name}</a>"

    t = t.replace(/\[([^\[\]]*)\]\((.*?)\)/gm, linkify)


    boldify = (x, s) ->
      bolding = false
      i = 0
      while i < s.length
        if i < s.length - 2 and (s[i] + s[i+1]) is "**"
          if bolding
            x += "</b>"
          else
            x += "<b>"
          bolding = not bolding
          i += 2
        else
          x += s[i]
          i++
      x
        
    t = boldify "", t


    italify = (x, s) ->
      italing = false
      i = 0
      while i < s.length
        if s[i] is "*"
          if italing
            x += "</i>"
          else
            x += "<i>"
          italing = not italing
          i++
        else
          x += s[i]
          i++
      x
        
    t = italify "", t

    #codift
    codify = (x) ->
      x = x.substring(3, x.length - 6).replace(/\ /gm, "&nbsp")
      "<div class='code'>#{x}</div>"


    t = t.replace(/\`\`\`(\s.*?)+?(?=\`)\`\`\`/gm, codify)


    # tabs
    t = t.split("\n").map((x) ->
      if x is ""
        "<br>"
        # ""
      else if x.substring( 0, 2) is "# "
        "<h1>#{x}</h1>"
      else
        leading_spaces = x.match(/^\ +/g)
        space_width = 0.6
        style = 
          if leading_spaces? 
            "style='margin-left : #{leading_spaces[0].length * space_width}em'"
          else ""

        l = if leading_spaces? then leading_spaces[0].length else 0

        # x = x.substring(l).replace(/\ /gm, "&nbsp;")

        "<div class='line' #{style}>#{x}</div>"

    ).join("")

    t