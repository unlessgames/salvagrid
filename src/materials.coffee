class Material
  constructor : (@name, @vert_name, @frag_name, @options = {}) ->
  @import_uniforms = (data) ->
    for k of data
      v = data[k]
      switch v.type
        when "vec3", "color"
          data[k].value = new THREE.Color v.value
    data
  update_uniform : (u, x) ->
    @material.uniforms[u].value = x
    @material.uniformsNeedUpdate = true

  add_uniform_gui : (n, constraints, initial, type) ->
    o = {}
    o[n] = initial

    finish = () => 
      @material.uniformsNeedUpdate = true
      local_save @material.export_uniforms(), "shader_#{@material.name}"

    update_fun = 
      switch type
        when "vec3"
          (v) => 
            @material.uniforms[n].value = new T.Color v
        when "float", "int" 
          (v) => 
            @material.uniforms[n].value = v
        else
          (v) =>
            @material.uniforms[n].value = v
    gu =
      switch type
         when "float", "int"
            @gui.add(o, n, constraints[0], constraints[1])
          when "color"
            @gui.addColor(o, n)
          when "bool"
            @gui.add(o, n)
          else
            @gui.add(o, n, constraints)

    gu.onChange(update_fun).onFinishChange(finish)

    update_fun(initial)
    finish()
  init_gui : (g) ->
    @gui = g.addFolder @name

    for k of @options.uniforms
      u = @options.uniforms[k]
      if u.type isnt "t"
        @add_uniform_gui k, [0..1], u.value, u.type
    
    @gui.open()
    @gui.hide()

  init : (@vert, @frag, saved_uniforms, gui) ->
    if saved_uniforms?
      for k of saved_uniforms
        @options.uniforms[k] = Material.import_uniforms saved_uniforms[k]
    @options.vertexShader = @vert
    @options.fragmentShader = @frag
    @options.name = @name


    @material = new THREE.ShaderMaterial @options
    @material.export_uniforms = () ->
      d = {}
      for k of @uniforms
        v = @uniforms[k]
        switch v.type
          when "float", "vec3", "bool"
            d[k] = {type : v.type, value : v.value}
      d


materials = 
  saturator : 
    new Material(
      "saturator"
      "texture.vert"
      "saturator.frag"
      uniforms : 
        saturation : 
          type : "float"
          value : 0.5
        brightness : 
          type: 'float'
          value: 0.5
        tex0 :
          type: "t"
          value: null
    )
  grayscale : 
    new Material(
      "grayscale"
      "texture.vert"
      "grayscale.frag"
      uniforms : 
        brightness : 
          type: 'float'
          value: 0.5
        invert : 
          type: "bool"
          value: true
        tex0 :
          type: "t"
          value: null 

    )


  screen : 
    new Material(
      "screen"
      "basic.vert"
      "sharp.frag"
      uniforms : 
        tex0 : 
          type : "t"
          value : null
      depthWrite: false
      side : THREE.DoubleSide
    )
  line : 
    new Material(
      "line"
      "line.vert"
      "line.frag"
      uniforms : 
        lineColor : 
          type : "vec3"
          value : new THREE.Color(options.line_color)
        time : 
          type: 'float'
          value: 0.0
    )


  files : {}

  mats_array : () ->
    Object.keys(@)
      .filter((k) -> ["init", "load", "mats_array", "files", "init_gui"].indexOf(k) is -1)
      .map((m) => @[m])

  load : (l) ->
    load_shader = (filename) =>
      l.load "shaders/" + filename, (text) => @files[filename] = text
    for m in @mats_array()
      load_shader m.vert_name
      load_shader m.frag_name

  init : () ->
    for m in @mats_array()
      m.init @files[m.vert_name], @files[m.frag_name], local_load("shader_#{m.name}"), gui
  init_gui : (g) ->
    for m in @mats_array()
      m.init_gui g
