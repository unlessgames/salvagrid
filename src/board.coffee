class Board
  toggle_preview : (show) ->
    if show?
      @preview.visible = show
    else
      @preview.visible = not @preview.visible
  line_geom : (ps) ->
    g = new THREE.BufferGeometry();
    arr = ps.reduce(((a, v, i) -> a.concat([v.x, v.y, 0, ps[(i + 1) % ps.length].x, ps[(i + 1) % ps.length].y, 0])), [])
    vs = new Float32Array(arr);
    g.setAttribute( 'position', new THREE.BufferAttribute( vs, 3 ) );
    g
  update_size : (w, h) ->
    @columns = w
    @rows = h

    @generate_geoms()
    @generate_table()

    window.save_board @index
  generate_geoms : () ->
    cs = @corners.map((c, i, arr) -> arr[arr.length - 1 - i])
    sw = options.screen.w
    sh = options.screen.h

    @pixels_geo = grid_geom cs, @columns, @rows, sw, sh
    @detail_geo = grid_geom(cs, @columns * options.oversample, @rows * options.oversample, sw, sh)
    @preview_geo = grid_geom(cs, @preview_size, @preview_size, sw, sh)
    @selection_geo = @line_geom @corners

    @pixels.geometry = @pixels_geo
    @detail.geometry = @detail_geo
    @preview.geometry = @preview_geo
    @selection.geometry = @selection_geo


    p = 0
    @pixels.position.set 0, p, 0
    p += @rows + 1
    @detail.position.set 0, p, 0
    p += @rows * options.oversample
    @preview.position.set 0, p, 2
    @selection.position.set 0, height - options.screen.h, 2

    # place letters
    @center = @corners.reduce ((a, v, i) -> new THREE.Vector2(a.x + v.x, a.y + v.y))
    @center.set @center.x / 4, @center.y / 4
    for p, i in @corners
      fs = options.font_size * (options.screen.h / 480)
      v = p.clone().sub(@center).normalize().multiplyScalar(fs * -1.5)
      v.add(p).add(new THREE.Vector2(-options.font_size * 0.4, -options.font_size * 0.5))
      @letters[i].position.set v.x, height - options.screen.h + v.y, -1

    tw = 200
    pw = 1 / options.plane_width
    ph = 1 / options.plane_height

    w = @columns
    h = @rows

    uvs = [
      new V2 0, 0
      new V2 pw * w, 0
      new V2 pw * w, ph * h
      new V2 0, ph * h
    ]
    @pixel_view.geometry = grid_geom(uvs, tw, tw, 1, 1)

    ww = pw * options.oversample * w
    hh = ph * options.oversample * h
    uvs = [
      new V2 0, ph * h + ph
      new V2 ww, ph * h + ph
      new V2 ww, ph * h + hh + ph
      new V2 0, ph * h + hh + ph
    ]
    @oversample_view.geometry = grid_geom(uvs, tw, tw, 1, 1)

    @pixel_view.position.set @preview_size + tw, 0, 0
    @oversample_view.position.set @preview_size, 0, -1

    @oversample_buffer = new Float32Array( (@rows * options.oversample) * (@columns * options.oversample) * 4);
    @pixels_buffer = new Float32Array( (@rows) * (@columns) * 4);

    @grid = 
      for x in [1..@columns]
        for y in [1..@rows]
          false
  send_cc : (cc, v) ->
    cc += @midi.start
    if @midi.output?
      channel = 
        if cc <= 119
          @midi.channel
        else
          @midi.channel + 1
      @midi.output.sendControlChange(cc % 119, v, channel)

  flush_cc : () ->
    for col, x in @grid
      for g, y in col
        @grid[x][y] = false
        @output_grid x, y

  drag : (i, x, y) ->
    ho = height - options.screen.h
    @corners[i].x = x
    @corners[i].y = y - ho
    @generate_geoms()
    @flush_cc()
    window.save_board @index

  manual_press_cell : (x, y) ->
    @forced = 
      x : x
      y : y
    @output_grid x, y
  manual_release : () ->
    if @forced?
      f = @forced
      @forced = null
      @table[f.x][f.y].removeClass "forced"  
      @output_grid f.x, f.y



  create_grid_element : () ->
    @table = [1..@columns].map (i) -> []

    table_element = $("<table>", 
      html : 
        [0..@rows - 1].map((y) => 
          $("<tr>",
            html : 
              [0..@columns - 1].map((x) => 
                i = @midi.start + y * @columns + x
                t = if i >= 119 then "^#{i % 119}" else i
                td = $("<td>", class : "grid-cell", html : t)
                  .mousedown (e) => @manual_press_cell x, (@rows - y - 1)
                  .mouseup (e) => @manual_release()
                @table[x].push td
                td
              )
          )
        )
    )
    @table = @table.map (r, y) => r.map((c, x, arr) => arr[arr.length - x - 1])
    @grid_element = $("<div>", class : "grid-container", html : table_element)
    @grid_element
  
  generate_table : () ->
    if @grid_element?
      @grid_element.remove()
    $($(@gui.domElement)).after @create_grid_element()
  panic : () ->
    @flush_cc()
  create_gui : (g) ->
    @gui = g.addFolder("GRID") ##{@index}")
    

    # @gui.addColor(@, 'frame_color').onChange((v) => @selection_material.color = new THREE.Color(v))
    @gui.add(@, 'columns', 1, 16).step(1).onChange((w) => @update_size(w, @rows))
    @gui.add(@, 'rows', 1, 16).step(1).onChange((h) => @update_size(@columns, h))
    @gui.open()

    @midi_gui = @gui.addFolder "MIDI"

    @midi_outputs = @midi_gui.add(@midi, "device", options.midi_outputs)
      .onChange (v) =>
        @midi.output = WebMidi.getOutputByName v
        window.save_board @index

    @midi_gui.add(@midi, "channel", 1, 16).step(1)
      .onChange (v) => 
        @flush_cc()
        window.save_board @index
    @midi_gui.add(@midi, "start", 0, 119).step(1)
      .onChange (v) =>
        @generate_table()
        @flush_cc()
        window.save_board @index

    # @gui.add(@, "remove").name "REMOVE"

    @midi_gui.add(@, "panic").name "panic!"
    @midi_gui.open()

    @generate_table()

  print_range : () ->
    "from #{@midi.start} to #{@rows * @columns - 1}"

  remove : () ->
    @gui.domElement.remove()
    @grid_element.remove()
    window.remove_grid @index

  refresh_midi_options : (ms) ->
    $(@midi_outputs.domElement.childNodes[0]).html(
      ms.map((m) -> $("<option>", html : m).attr("value", m))
    )
    if ms.indexOf(@midi.device) is -1
      @midi.device = ms[0]


  check_oversampled : (x, y, threshold = 1) ->
    i = ((y * @columns * 4) * options.oversample + (x * 4)) * options.oversample
    gc = 0
    for jx in [0..2]
      for jy in [0..2]
        _y = (jy * (@columns * options.oversample * 4))
        p = i + (jx * 4) + _y
        b = @oversample_buffer[p] + @oversample_buffer[p + 1] + @oversample_buffer[p + 2]
        if b > 0
          gc++;
          if gc > threshold
            return true
    return false

  output_grid : (x, y) ->
    g = 
      if @forced? and @forced.x is x and @forced.y is y
        @table[x][y].addClass "forced"

        true
      else
        @grid[x][y]
    @table[x][y].toggleClass "active", g
    @send_cc((x + (@rows - y - 1) * @columns), (if g then 127 else 0))

  update_grid : () ->
    renderer.readRenderTargetPixels(
      render_target
      0, 
      @rows,
      @columns * options.oversample,
      @rows * options.oversample,
      @oversample_buffer
    )
    renderer.readRenderTargetPixels(
      render_target
      0, 
      0,
      @columns, 
      @rows, 
      @pixels_buffer
    )

    for col, x in @grid
      for t, y in col
        y = @rows - y - 1
        i = y * @columns * 4 * 1 + x * 4
        g = @pixels_buffer[i] + @pixels_buffer[i+1] + @pixels_buffer[i+2] > 0
        if not g and @grid[x][y]
          g = @check_oversampled(x, y, 2)
        else
          if not g
            g = @check_oversampled(x, y, 2)
        if g isnt @grid[x][y]
          @grid[x][y] = g
          @output_grid(x, y)
  save : () ->
    midi : 
      device : @midi.device
      channel : @midi.channel
      start : @midi.start
    corners : 
      @corners.map((v) -> {x : v.x, y : v.y})
    columns : @columns
    rows : @rows
  constructor : (data, @materials, @index = 0, gui) ->
    @columns = data.columns 
    @rows = data.rows
    @midi = data.midi
    @corners = data.corners.map((v) -> new THREE.Vector2(v.x, v.y))
    @midi.output = WebMidi.getOutputByName @midi.device

    @frame_color = "#0f0"

    @preview_size = 128
    dummy_geo = new THREE.Geometry()
    @pixels = new THREE.Mesh dummy_geo, @materials[0] 
    @detail = new THREE.Mesh dummy_geo, @materials[0]
    @preview = new THREE.Mesh dummy_geo, @materials[0]


    
    @selection = new THREE.Line(dummy_geo, @materials[2])

    # @letter_mat = new T.MeshBasicMaterial(color : @frame_color)

    @letters = @corners.map (p, i) => 
      new T.Mesh window.corner_letters[i], @materials[2]

    @pixel_view = new THREE.Mesh(dummy_geo , @materials[1] )
    @oversample_view = new THREE.Mesh( dummy_geo, @materials[1] )
    
    @generate_geoms()


    @objects = [ 
      @pixels
      @detail
    ]
    @screen_objects = [
      @selection
      @pixel_view
      @oversample_view
      @preview
    ]
    @screen_objects = @screen_objects.concat @letters
    @create_gui(gui)
    # else
    #   alert "you'd need at least 1 midi output active to use this site"