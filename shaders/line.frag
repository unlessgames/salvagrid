uniform vec3 lineColor;
uniform float time;

vec3 rgb2hsb(vec3 c){
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
    vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsb2rgb(vec3 c){
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {

  // vec2 uv = vUv;

  // vec4 tex = texture2D(tex0, uv);
  float tr = (time > 0.5 ? 1.0 - time : time);
  vec3 hsb = rgb2hsb(lineColor);
  hsb.r = tr * 2.0;
  hsb.b = 0.5 + tr;
  // gl_FragColor = vec4(lineColor,1);
  gl_FragColor = vec4(hsb2rgb(hsb), 1);
}