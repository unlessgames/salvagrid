varying vec3 vUv; 
// varying vec2 vTexCoord;
void main() {
  vUv =  vec3(uv, 0); 
  vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
  gl_Position = projectionMatrix * modelViewPosition; 
}