uniform vec3 background; 
uniform float saturation;
uniform float brightness;
uniform vec3 foreground; 
uniform sampler2D tex0;
uniform bool invert;
varying vec3 vUv;

vec3 rgb2hsb(vec3 c){
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
    vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}
vec3 hsb2rgb(vec3 c){
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

const int w = 4;
vec3 average_field(vec2 p){
  vec3 t = texture2D(tex0, p).rgb;
  vec3 c = vec3(0,0,0);
  float s = 0.01;
  float ws = float(w) * 0.5 * s;
  p = vec2(p.x - ws, p.y - ws);
  for(int x = 0; x < w; x++){
    for(int y = 0; y < w; y++){
      float _x = float(x) * s;
      float _y = float(y) * s;
      vec3 px = texture2D(tex0, p + vec2(_x, _y)).rgb;
      vec3 hsb = rgb2hsb(px);
      if(hsb.b > brightness){
        hsb.b = 1.0;
        c += hsb2rgb(hsb);
      }
    }
  }
  return c;
}

float lightness(vec3 c){
  float m = min(min(c.r, c.g), c.b);
  return m;
}

void main() {
  // vec4 t = texture2D(tex0, vUv.xy);
  vec3 c = average_field(vUv.xy);
  float a = float(w * w);
  c = vec3(c.r / a, c.g / a, c.b / a);

  float l = lightness(c);

  float v = invert ? 0.0 : 1.0;

  if(l > brightness)
    c = vec3(v);
  else 
    c = vec3(1.0 - v);

  gl_FragColor = vec4(c, 1);
  // gl_FragColor = t;
}